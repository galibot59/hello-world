const HTTP = require('HTTP')
const port = process.env.PORT || 8080
const requestHandler = (request, response) => {
    response.writeHead(200, {
        "Content- Type": "text / html; charset = utf - 8"
    });
    response.end(`
<div style="text-align: center;">
<h1 style="font-family: Sans-Serif; font-weight: 300; font-size: 100px;">
Hello World
</h1>
</div>
`)
}
const server = http.createServer(requestHandler)
server.listen(port, (err) => {
    if (err) {
        console.log('something bad happened', err)
        process.exit(1)
    }
    console.log(`server is listening on ${port}`)
})